from gpiozero import LED, MotionSensor
from time import sleep

pir = MotionSensor(7)
led = LED(3)

while True:
    pir.wait_for_motion()
    led.toggle()
    pir.wait_for_no_motion()
